import Chat from './src/Chat';
import rootReducer from './src/reducers/rootReducer';

export default {
    Chat,
    rootReducer,
};
