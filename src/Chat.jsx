import React from 'react';
import { connect } from 'react-redux';

import MessageList from './components/MessageList/MessageList';
import MessageInput from './components/MessageInput/MessageInput';
import { Preloader } from './components/Preloader/Preloader';
import { Header } from './components/Header/Header';

import * as Action from './actions/actions';

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            loading: true,
            participants: 0,
            lastMessageTime: Date.now(),

        }
        // this.onMessageAdd = this.onMessageAdd.bind(this);
        // this.onMessageEdit = this.onMessageEdit.bind(this);
        // this.onMessageDelete = this.onMessageDelete.bind(this);
        this.onMessageLike = this.onMessageLike.bind(this);
        this.initStorage = this.initStorage.bind(this);
    }

    componentDidMount() {
        fetch(this.props.url)
            .then(response => response.json())
            .then(data => this.props.initStorage(
                {
                    ...this.props,
                    messages: data.map(m => ({ ...m, liked: false })),
                    loading: false,
                    lastMessageTime: data[data.length - 1].createdAt,
                    participants: new Set(data.map(message => message.userId)).size,
                    editModal: false,
                }))
            .catch((err) => {
                console.log(err);
                this.props.setLoadingMode(false);
            });
        this.props.setLoadingMode(false);
    }




    onMessageLike = (id, state) => {
        this.props.likeMessage(id, state);
    }

    initStorage = (state) => {
        this.props.initStorage(state);
    }

    render() {
        const { messages, loading, participants, lastMessageTime } = this.props;
        return (
            <div className="chat">
                {loading || messages === null || messages === undefined ?
                    (<Preloader />) : <>
                        <Header participants={participants} messagesCount={messages.length} lastMessageTime={lastMessageTime} />
                        <MessageList messages={messages}
                            likeMessage={this.likeMessage} />
                        <MessageInput
                            sendMessage={this.sendMessage} />
                    </>
                }
            </div>

        );
    }

}

const mapStateToProps = (state) => ({
    ...state
})

const mapDispatchToProps = {
    initStorage: Action.initStorage,
    onMessageAdd: Action.addMessage,
    onMessageLike: Action.likeMessage,
    setLoadingMode: Action.setLoading,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat)