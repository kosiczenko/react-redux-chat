import * as Action from './actionTypes';
import uuid from 'react-uuid'

export const initStorage = (initialState) => ({
    type: Action.STORAGE_INIT,
    payload: {
        initialState,
    },
});

export const addMessage = (message) => ({
    type: Action.MESSAGE_ADD,
    payload:
    {
        id: uuid(),
        message,
    }
});

export const editMessage = (message) => ({
    type: Action.MESSAGE_EDIT,
    payload: message
});

export const deleteMessage = (messageId) => ({
    type: Action.MESSAGE_DELETE,
    payload: messageId
});

export const likeMessage = (messageId) => ({
    type: Action.MESSAGE_LIKE,
    payload: messageId
});

export const formToggleModal = () => ({
    type: Action.FORM_TOGGLE_MODAL,
});

export const setLoading = (isLoading) => ({
    type: Action.SET_LOADING_MODE,
    payload: isLoading
});

