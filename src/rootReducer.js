import * as Action from './actions/actionTypes';

export default function rootReducer(state, action) {

    switch (action.type) {

        case Action.STORAGE_INIT: {
            return { ...action.payload.initialState };
        }

        case Action.MESSAGE_ADD: {
            const message = action.payload.message;
            const newMessages = [state.messages, message];
            return { ...state, messages: newMessages };
        }

        case Action.MESSAGE_DELETE: {
            const messageId = action.payload;
            const newMessages = state.messages.filter(message => message.id !== messageId);
            return { ...state, messages: newMessages };
        }

        case Action.MESSAGE_EDIT:
            const editedMessage = action.payload;

            return {
                ...state, messages: state.messages.map(message =>
                    message.id === messageId
                        ? editedMessage
                        : message)
            };

        case Action.MESSAGE_LIKE:
            const messageId = action.payload;
            const messagesWithLiked = state.messages.map(message =>
                message.id === messageId
                    ? { ...message, liked: !message.liked }
                    : message);

            return {
                ...state, messages: messagesWithLiked,
            };

        case Action.FORM_TOGGLE_MODAL: {
            return { ...state, editModal: !state.editModal };
        }
        case Action.SET_LOADING_MODE: {
            const loading = action.payload;
            return { ...state, loading: loading };
        }

        default:
            return state;
    }
}
