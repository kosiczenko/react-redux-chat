import CircularProgress from '@material-ui/core/CircularProgress';
function Preloader() {
    return <div className="preloader">
        <CircularProgress />
    </div>
}

export { Preloader }