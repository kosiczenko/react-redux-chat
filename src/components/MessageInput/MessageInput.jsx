import React from 'react'
import Button from '@material-ui/core/Button';
import uuid from 'react-uuid';
import { connect } from 'react-redux';
import * as Action from '../../actions/actions';

class MessageInput extends React.Component {
    constructor() {
        super()
        this.state = {
            message: ''
        }
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(e) {
        this.setState({
            message: e.target.value,
        })
    }

    onMessageAdd = (text) => {
        if (text === undefined || text === "") {
            return;
        }

        //var id = document.getElementById("editMessageId").value;
        // if (id !== undefined && id !== "") {
        //     const messageList = this.state.messages.map(message =>
        //     (message.id !== id ?
        //         message :
        //         {
        //             ...message,
        //             text: text,
        //             editedAt: new Date()
        //         }
        //     )
        //     );
        //     // this.setState({ messages: messageList });

        //     return;
        // }



        //   var newMessages = [...this.state.messages, newMessage];

        // this.setState({
        //     messages: newMessages,
        //     lastMessageTime: messageTime,
        //     participants: new Set(newMessages.map(message => message.userId)).size
        // });

    }

    handleSubmit(e) {
        // e.preventDefault();
        const messageTime = Date();

        const newMessage = {
            "id": uuid(),
            "avatar": this.props.userAvatar,
            "text": this.state.message,
            "createdAt": messageTime,
            "user": this.props.userName,
            "userId": this.props.userId,
        };

        this.props.onMessageAdd(newMessage);
        // this.setState({
        //     message: ''
        // });
        var textEdit = document.getElementsByClassName("message-input-text")[0];
        textEdit.value = '';
    }

    render() {
        return (
            <form className="send-message-form">
                <div className="message-input">
                    <input className="message-input-text" placeholder="Message" onChange={this.handleChange}>
                    </input>

                    <Button className="message-input-button"
                        color="primary"
                        onClick={this.handleSubmit} >Send</Button>
                    <input id="editMessageId" type="hidden" />
                </div >
            </form >
        )
    }

}
const mapStateToProps = (state) => ({
    ...state
})

const mapDispatchToProps = {
    onMessageAdd: Action.addMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput)
