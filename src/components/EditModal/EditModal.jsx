import React from 'react'

class EditModal extends React.Component {
    constructor() {
        super()
        this.state = {
            message: ''
        }
        this.handleEdit = this.handleEdit.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleTyping = this.handleTyping.bind(this);
    }

    componentDidMount() {
        const messageId = this.props.messageId;
        const currMessage = this.props.messages.find((message) => message.id === messageId);
        this.setState({ text: currMessage.text });
    }

    handleChange(event) {
        this.setState({ message: event.target.value })
    }

    handleSubmit(e) {
        this.props.editMessage(this.state.message);
        this.setState({ message: '' });
        var textEdit = document.getElementsByClassName("edit-message-input")[0];
        textEdit.value = '';
    }

    handleClose() {
        this.props.hideModal();
    }

    render() {
        const { text } = this.state;
        return (
            <div className="edit-message-modal" >
                <div className="modal-shown">
                    <div className="modal-header">
                        <span>Edit Message</span>
                    </div>
                    <div className="modal-body">
                        <div className="edit-form">
                            <input type="text" className="edit-message-input" value={text} onChange={this.handleChange} />
                            <button className="edit-message-button" onClick={() => this.handleEdit()}>Ok</button>
                            <button className="edit-message-close" onClick={() => this.handleClose()}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
            // <form className="send-message-form">
            //     <div className="message-input">
            //         <input className="message-input-text" placeholder="Message" onChange={this.handleChange}>
            //         </input>

            //         <Button className="message-input-button"
            //             color="primary"
            //             onClick={this.handleSubmit} >Send</Button>
            //         <input id="editMessageId" type="hidden" />
            //     </div >
            // </form >
        )
    }

}

export { EditModal }