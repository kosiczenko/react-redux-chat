import React from "react";
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { connect } from "react-redux";
import * as Action from '../../actions/actions';

class OwnMessage extends React.Component {

    onMessageEdit = (id) => {
        this.props.setMessageId(id);
        this.props.showPage();
    }

    onMessageDelete = (id) => {
        this.props.deleteMessage(id);
    }

    render() {
        if (this === null) {
            return <> </>;
        }

        const { id, avatar, user, text, createdAt } = this.props;
        var dateFormat = require('dateformat');
        var messageDate = dateFormat(Date.parse(createdAt), "HH:MM", true);
        return (
            <div className="message-text">
                <ListItem id={id} alignContent="flex-end" className="own-message">
                    <ListItemAvatar>
                        <Avatar className="avatar" alt={user} src={avatar} />
                        <Typography variant="h7" color="initial" >{user}</Typography>
                    </ListItemAvatar>
                    <ListItemText primary={text} secondary={messageDate} />
                    <IconButton aria-label="Edit" color="primary" onClick={() => this.props.editMessage(this.props.message)} >
                        <EditIcon />
                    </IconButton>
                    <IconButton aria-label="Delete" color="primary" onClick={() => this.props.deleteMessage(id)} >
                        <DeleteIcon />
                    </IconButton>
                </ListItem>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    ...state
})

const mapDispatchToProps = {
    onMessageAdd: Action.addMessage,
    onMessageEdit: Action.editMessage,
    onMessageDelete: Action.deleteMessage,
}

export default connect(mapStateToProps, mapDispatchToProps)(OwnMessage)
