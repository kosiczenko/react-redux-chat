import React from "react";
import { connect } from "react-redux";
import { Avatar, IconButton, Typography } from "@material-ui/core";
import ThumbUp from '@material-ui/icons/ThumbUp';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemText from '@material-ui/core/ListItemText';
import { likeMessage } from "../../actions/actions";

class Message extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            liked: false,
        }
    }

    onMessageLike() {
        this.props.likeMessage(this.props.id);
        this.setState({ liked: !this.props.liked });
    }

    render() {
        const { id, avatar, user, text, createdAt, liked } = this.props;

        var btnColor;
        var className;

        if (liked) {
            btnColor = "primary";
            className = "message-liked";
        } else {
            btnColor = "disabled";
            className = "message-like"
        }
        var dateFormat = require('dateformat');
        var messageDate = dateFormat(Date.parse(createdAt), "HH:MM", true);

        return <div id={id} className="message">
            <div className="message-text">
                <ListItem>
                    <ListItemAvatar>
                        <Avatar className="avatar" alt={user} src={avatar} />
                        <Typography variant="h7" color="initial" >{user}</Typography>
                    </ListItemAvatar>
                    <ListItemText primary={text} />
                    <div className="message-time">{messageDate}</div>
                    <div className={className}>
                        <IconButton color={btnColor} aria-label="Like" component="span" onClick={() => this.onMessageLike()} >
                            <ThumbUp />
                        </IconButton >
                    </div>
                </ListItem>
            </div >
        </div>
    }
}

const mapDispatchToProps = {
    likeMessage,
};

export default connect(null, mapDispatchToProps)(Message);
